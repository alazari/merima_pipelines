#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts

outDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed/
mkdir -p $outDir
mkdir -p $outDir/T2w
exportDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed_export
mkdir -p $exportDir
mkdir -p $exportDir/T2w
mkdir -p $exportDir/T2w/MNC


subjList="MS07@20211008_154740_MS07_Merima_Doldcr_T2w_Diffusion_1_1
MS08@20211011_201821_MS08_Merima_Doldcr_T2w_Diffusion_1_1
MS09@20211012_174125_MS09_Merima_Doldcr_T2w_Diffusion_1_2
MS10@20211013_180206_MS10_Merima_Doldcr_T2w_Diffusion_1_1
MS11@20211014_162422_MS11_Merima_Doldcr_T2w_Diffusion_1_1
MS12@20211015_180125_MS12_Merima_Doldcr_T2w_Diffusion_1_1
MS13@20211018_175940_MS13_Merima_Doldcr_T2w_Diffusion_1_2
MS14@20211019_174300_MS14_Merima_Doldcr_T2w_Diffusion_1_2
MS15@20211021_200506_MS15_Merima_Doldcr_T2w_Diffusion_1_1
MS16@20211022_151312_MS16_Merima_Doldcr_T2w_Diffusion_1_2
MS17@20211026_172625_MS17_Merima_Doldcr_T2w_Diffusion_1_1
MS18@20211027_203821_MS18_Merima_Doldcr_T2w_Diffusion_1_1
MS19@20211029_154024_MS19b_Merima_Doldcr_T2w_Diffusion_1_1
MS22@20211108_194315_MS22_Merima_Doldcr_T2w_Diffusion_4_1_5
MS23@20211103_181935_MS23_Merima_Doldcr_T2w_Diffusion_1_1
MS25@20211104_155453_MS25_Merima_Doldcd_T2w_Diffusion_1_1
MS26@20211109_155744_MS26_Merima_Doldcr_T2w_Diffusion_1_1"

# T2w is number 7
# MS02@20210930_183625_MS02_MS02_Merima_Doldcr_T2w_Diffusion_1_1
# MS03@20211004_193158_MS03_MS03_Merima_Doldcr_T2w_Diffusion_1_1
# MS04@20211006_163600_MS04b_MS04_Merima_Doldcr_T2w_Diffusion_1_1
# MS06@20211007_161034_MS06_MS06_Merima_Doldcr_T2w_Diffusion_1_1

# T2w is number 5
# MS07@20211008_154740_MS07_Merima_Doldcr_T2w_Diffusion_1_1
# MS08@20211011_201821_MS08_Merima_Doldcr_T2w_Diffusion_1_1
# MS09@20211012_174125_MS09_Merima_Doldcr_T2w_Diffusion_1_2
# MS10@20211013_180206_MS10_Merima_Doldcr_T2w_Diffusion_1_1
# MS11@20211014_162422_MS11_Merima_Doldcr_T2w_Diffusion_1_1
# MS12@20211015_180125_MS12_Merima_Doldcr_T2w_Diffusion_1_1
# MS13@20211018_175940_MS13_Merima_Doldcr_T2w_Diffusion_1_2
# MS14@20211019_174300_MS14_Merima_Doldcr_T2w_Diffusion_1_2
# MS15@20211021_200506_MS15_Merima_Doldcr_T2w_Diffusion_1_1
# MS16@20211022_151312_MS16_Merima_Doldcr_T2w_Diffusion_1_2
# MS17@20211026_172625_MS17_Merima_Doldcr_T2w_Diffusion_1_1
# MS18@20211027_203821_MS18_Merima_Doldcr_T2w_Diffusion_1_1
# MS19@20211029_154024_MS19b_Merima_Doldcr_T2w_Diffusion_1_1
# MS22@20211108_194315_MS22_Merima_Doldcr_T2w_Diffusion_4_1_5
# MS23@20211103_181935_MS23_Merima_Doldcr_T2w_Diffusion_1_1
# MS25@20211104_155453_MS25_Merima_Doldcd_T2w_Diffusion_1_1
# MS26@20211109_155744_MS26_Merima_Doldcr_T2w_Diffusion_1_1

#"MS81_3_20200903"

# "MS81_10_20200904
# MS82_3_20200914
# MS86_10_20200924
# MS81_1_20200901
# MS82_N_20200911
# MS86_1_20200921
# MS81_3_20200903
# MS84_10_20200918
# MS86_3_20200923
# MS81_N_20200907
# MS84_1_20200916
# MS86_N_20200925
# MS82_10_20200915
# MS84_3_20200917
# MS82_1_20200910
# MS84_N_20200909"

module add minc-toolkit-v1

for subjScan in $subjList; do

  # separate the subject and the scan number
  subj=${subjScan%@*}
  scan=${subjScan#*@}

  echo "conversion to mnc format for $subj"

  # /vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/unring $workDir/"$subj"/T2w/MS81_3_Merima_Doldcr_T2w_Diffusion_Part2_7_1_1.nii $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed

  # /vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/unring $workDir/"$subj"/T2w/"$subj"_T2w_anat.nii $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed

  /vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/unring $workDir/"$scan"/5/pdata/1/nifti/"$subj"*.nii $outDir/T2w/"$subj"_T2w_anat_unringed

  #cp $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed.nii.gz $exportDir/T2w/"$subj"_T2w_anat_unringed.nii.gz

  nii2mnc -quiet -signed -float $outDir/T2w/"$subj"_T2w_anat_unringed.nii $exportDir/T2w/MNC/"$subj"_T2w_anat.mnc

echo "done"

done
