#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/myelin/Mohamed/Merima_data_firstbatch_imported
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts

mncDir=/vols/Scratch/myelin/Mohamed/Merima_data_firstbatch_imported/MNC
mkdir -p $mncDir
mkdir -p $mncDir/T2w
mkdir -p $mncDir/DWI
exportDir=/vols/Scratch/myelin/Mohamed/Merima_data_firstbatch_imported/Export
mkdir -p $exportDir
mkdir -p $exportDir/T2w
mkdir -p $exportDir/DWI

subjList="MS81_3_20200903"


#"MS81_3_20200903"

# "MS81_10_20200904
# MS82_3_20200914
# MS86_10_20200924
# MS81_1_20200901
# MS82_N_20200911
# MS86_1_20200921
# MS81_3_20200903
# MS84_10_20200918
# MS86_3_20200923
# MS81_N_20200907
# MS84_1_20200916
# MS86_N_20200925
# MS82_10_20200915
# MS84_3_20200917
# MS82_1_20200910
# MS84_N_20200909"

module add minc-toolkit-v1

for subj in $subjList; do

  echo "conversion to mnc format for $subj"

  # /vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/unring $workDir/"$subj"/T2w/MS81_3_Merima_Doldcr_T2w_Diffusion_Part2_7_1_1.nii $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed

  /vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/unring $workDir/"$subj"/T2w/"$subj"_T2w_anat.nii $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed

  cp $workDir/"$subj"/T2w/"$subj"_T2w_anat_unringed.nii.gz $exportDir/T2w/"$subj"_T2w_anat_unringed.nii.gz

  # cp $workDir/"$subj"/dtifit/"$subj"_FA.nii.gz $exportDir/DWI/"$subj"_FA.nii.gz
  # cp $workDir/"$subj"/dtifit/"$subj"_MD.nii.gz $exportDir/DWI/"$subj"_MD.nii.gz
  # cp $workDir/"$subj"/DWI_all/b0_mean.nii.gz $exportDir/DWI/"$subj"_b0_mean.nii.gz

  # nii2mnc -quiet -signed -float $workDir/"$subj"/T2w/"$subj"_T2w_anat.nii $mncDir/T2w/"$subj"_T2w_anat.mnc
  #
  # nii2mnc -quiet -signed -float $workDir/"$subj"/dtifit/"$subj"_FA.nii.gz $mncDir/DWI/"$subj"_FA.mnc
  # nii2mnc -quiet -signed -float $workDir/"$subj"/dtifit/"$subj"_MD.nii.gz $mncDir/DWI/"$subj"_MD.mnc
  # nii2mnc -quiet -signed -float $workDir/"$subj"/DWI_all/b0_mean.nii.gz $mncDir/DWI/"$subj"_b0_mean.mnc

echo "done"

done
