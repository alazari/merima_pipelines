# Mini-Guide

# T2w 

Just run the Convert_2_MNC_T2w.sh script

# DIFFUSION

Relevant scripts:
wrapper_Cristiana_pipeline.sh
wrapper_NODDI_and_b0.sh
copy_niftis_and_mncs.sh

STEP 1. Run Cristiana's wrapper (diffpostproc_pipeline_full.sh)

STEP 2. Extract average b0 + run Amy's run_NODDI.sh (wrapper_NODDI_and_b0.sh)

STEP 3. Run Cristiana's clean-up script to free up space in the directories

STEP 4. Run wrapper to copy files and convert to mnc (copying ODI and fintra/NDI,  but not fiso)

STEP 6. Offline QC and import to Graham for registration of average b0s

# QSM

# 2 scans with 20 echoes each

# Notes: b0 needed to register the QSM echoes to it
# It's fine for the 2 scans to be shifted from each other, the pipeline contains a registration step
# Select a good echo that you can use with MBM first

STEP 1. Run registration with MBM on one of the echoes, so you can get a good brain mask for each subject's native space. Use the same initial model as for the diffusion data.
# Picking a good input for MBM: try average of first 4 echoes, but sometimes that doesn't work well. Then try either the second echo (high SNR and no bubble distortion, but low contrast), or 4th/5th echo (high contrast but low SNR and more distortion from bubbles)
# Also, sometimes the voted/segmentation file from the nlin (i.e. study template) has better segmentation, so you can backproject that to native space.

STEP 2. Convert mask to nifti and expand it.

STEP 3. Run Cristiana's pipeline.