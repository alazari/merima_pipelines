#!/usr/bin/env bash

# definitions
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts/

subjList="MS02@20210930_183625_MS02_MS02_Merima_Doldcr_T2w_Diffusion_1_1
MS03@20211004_193158_MS03_MS03_Merima_Doldcr_T2w_Diffusion_1_1
MS04@20211006_163600_MS04b_MS04_Merima_Doldcr_T2w_Diffusion_1_1
MS06@20211007_161034_MS06_MS06_Merima_Doldcr_T2w_Diffusion_1_1
MS07@20211008_154740_MS07_Merima_Doldcr_T2w_Diffusion_1_1
MS08@20211011_201821_MS08_Merima_Doldcr_T2w_Diffusion_1_1
MS09@20211012_174125_MS09_Merima_Doldcr_T2w_Diffusion_1_2
MS10@20211013_180206_MS10_Merima_Doldcr_T2w_Diffusion_1_1
MS11@20211014_162422_MS11_Merima_Doldcr_T2w_Diffusion_1_1
MS12@20211015_180125_MS12_Merima_Doldcr_T2w_Diffusion_1_1
MS13@20211018_175940_MS13_Merima_Doldcr_T2w_Diffusion_1_2
MS14@20211019_174300_MS14_Merima_Doldcr_T2w_Diffusion_1_2
MS15@20211021_200506_MS15_Merima_Doldcr_T2w_Diffusion_1_1
MS16@20211022_151312_MS16_Merima_Doldcr_T2w_Diffusion_1_2
MS17@20211026_172625_MS17_Merima_Doldcr_T2w_Diffusion_1_1
MS18@20211027_203821_MS18_Merima_Doldcr_T2w_Diffusion_1_1
MS19@20211029_154024_MS19b_Merima_Doldcr_T2w_Diffusion_1_1
MS22@20211108_194315_MS22_Merima_Doldcr_T2w_Diffusion_4_1_5
MS23@20211103_181935_MS23_Merima_Doldcr_T2w_Diffusion_1_1
MS25@20211104_155453_MS25_Merima_Doldcd_T2w_Diffusion_1_1
MS26@20211109_155744_MS26_Merima_Doldcr_T2w_Diffusion_1_1"


# MS80@20210312_192904_MS80_Merima_Doldcr_T2w_Diffusion_1_3
# MS74@20210301_160217_MS74_MS74_MCAO_T2w_Diffusion_QSM_round2_1_3
# MS75@20210302_195833_MS75_MS74_MCAO_T2w_Diffusion_QSM_1_2
# MS95@20210315_184117_MS95_MS95_Merima_Doldcr_T2w_Diffusion_1_1
# MS76@20210303_164834_MS76_MS76_MCAO_T2w_Diffusion_QSM_round2_1_2
# MS78@20210304_165220_MS78_MS78_MCAO_T2w_Diffusion_QSM_round2_1_2
# MS77@20210316_193017_MS77_MS77_Merima_Doldcr_T2w_Diffusion_1_3
# MS93@20210305_205129_MS93_N_Merima_Doldcr_T2w_Diffusion_1_1
# MS86@20210308_191125_MS86_Merima_Doldcr_T2w_Diffusion_1_2
# MS94@20210317_190003_MS94_MS94_Merima_Doldcr_T2w_Diffusion_1_3
# MS88@20210309_174418_MS88_Merima_Doldcd_T2w_Diffusion_1_1
# MS79@20210311_174652_MS79_MS79_Merima_Doldcd_T2w_Diffusion_1_3
# MS73@20210322_155431_MS73_MS73_Merima_Doldcd_T2w_Diffusion_1_5
# MS87@20210323_182201_MS87_MS87_Merima_Doldcd_Tw2_Diffusion_1_6
# MS85@20210319_194410_MS85_MS85_Merima_Doldcr_T2w_Diffusion2_1_4

for subjScan in $subjList; do

  # separate the subject and the scan number
  subj=${subjScan%@*}
  scan=${subjScan#*@}

  echo "copying and converting files for $subj"

  workDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed/"$scan"
  outDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed_export/NIFTI/"$subj"
  outDirMNC=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed_export/MNC/"$subj"
  mkdir -p $outDir
  mkdir -p $outDirMNC

  # copy the average b0 (input for MBM for diffusion)
  cp $workDir/data_gibbs_eddy_b0_mean.nii.gz $outDir/"$subj"_data_gibbs_eddy_b0_mean.nii.gz
  #
  # # copy the DTIFIT outputs
  # cp $workDir/dtifit_gibbs_eddy/dtifit_gibbs_eddy_MD.nii.gz $outDir/"$subj"_MD.nii.gz
  # cp $workDir/dtifit_gibbs_eddy/dtifit_gibbs_eddy_FA.nii.gz $outDir/"$subj"_FA.nii.gz
  # cp $workDir/dtifit_gibbs_eddy/dtifit_gibbs_eddy_V1.nii.gz $outDir/"$subj"_V1.nii.gz
  #
  # # copy the NODDI outputs
  # cp $workDir/data_noddi_bpx.NODDI_Watson_diff_exvivo/mean_fintra.nii.gz $outDir/"$subj"_mean_fintra_56.nii.gz
  # cp $workDir/data_noddi_bpx.NODDI_Watson_diff_exvivo/OD.nii.gz $outDir/"$subj"_OD_56.nii.gz
  #
  # # convert everything to MNC
  # gunzip $outDir/"$subj"_data_gibbs_eddy_b0_mean.nii.gz
  # nii2mnc $outDir/"$subj"_data_gibbs_eddy_b0_mean.nii $outDirMNC/"$subj"_data_gibbs_eddy_b0_mean.mnc
  #
  # gunzip $outDir/"$subj"_MD.nii.gz
  # nii2mnc $outDir/"$subj"_MD.nii $outDirMNC/"$subj"_MD.mnc
  #
  # gunzip $outDir/"$subj"_V1.nii.gz
  # nii2mnc $outDir/"$subj"_V1.nii $outDirMNC/"$subj"_V1.mnc
  #
  # gunzip $outDir/"$subj"_FA.nii.gz
  # nii2mnc $outDir/"$subj"_FA.nii $outDirMNC/"$subj"_FA.mnc
  #
  # gunzip $outDir/"$subj"_mean_fintra_56.nii.gz
  # nii2mnc $outDir/"$subj"_mean_fintra_56.nii $outDirMNC/"$subj"_mean_fintra_56.mnc
  #
  # gunzip $outDir/"$subj"_OD_56.nii.gz
  # nii2mnc $outDir/"$subj"_OD_56.nii $outDirMNC/"$subj"_OD_56.mnc

  # copy the new NODDI outputs
  cp $workDir/data_noddi_bpx.NODDI_Watson_diff_exvivo/mean_fintra.nii.gz $outDir/"$subj"_mean_fintra_47.nii.gz
  cp $workDir/data_noddi_bpx.NODDI_Watson_diff_exvivo/OD.nii.gz $outDir/"$subj"_OD_47.nii.gz

  gunzip $outDir/"$subj"_mean_fintra_47.nii.gz
  nii2mnc $outDir/"$subj"_mean_fintra_47.nii $outDirMNC/"$subj"_mean_fintra_47.mnc

  gunzip $outDir/"$subj"_OD_47.nii.gz
  nii2mnc $outDir/"$subj"_OD_47.nii $outDirMNC/"$subj"_OD_47.mnc

echo "done"

done
