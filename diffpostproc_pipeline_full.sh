#!/bin/bash
module add mrdegibbs3d

if [ $# -lt 4 ]
then
    echo "Usage: $0 inputdir exp_no_shell1 exp_no_shell2 exp_no_blipdown outputdir "
    exit 0
fi

orient_corr () {
fslorient -deleteorient $1
fslswapdim $1 z -y x $1
fslorient -setsform 0.1 0 0 0 0 0.1 0 0 0 0 0.1 0 0 0 0 1 $1
fslorient -copysform2qform $1
fslorient -setsformcode 1 $1
fslorient -setqformcode 1 $1
}

inputdir=$1
shell1=$2
shell2=$3
blipDown=$4
outputdir=$5

if [ -d "${outputdir}" ];
then
  echo "Output directory exists. Will work there."
else
  mkdir $5
fi

foldername=$(echo `basename ${inputdir}`)
folderlength=`echo -n $foldername | wc -c`
((folderlength = folderlength - 4))
substring=$(echo `basename ${inputdir}` | cut -b 17-${folderlength})

nofiles_shell1=`ls ${inputdir}/${shell1}/pdata/1/nifti/ -1 | wc -l`
nofiles_shell2=`ls ${inputdir}/${shell2}/pdata/1/nifti/ -1 | wc -l`
((nofiles = $nofiles_shell1 + $nofiles_shell2))

echo merging all files
for i in `seq 1 $nofiles_shell1`
do
  printf "$inputdir/${shell1}/pdata/1/nifti/${substring}_${shell1}_1_${i}.nii "  >> $outputdir/filenames
done

for i in `seq 1 $nofiles_shell2`
do
  printf "$inputdir/${shell2}/pdata/1/nifti/${substring}_${shell2}_1_${i}.nii "  >> $outputdir/filenames
done

filelist=`cat $outputdir/filenames`

fslmerge -t ${outputdir}/data_merged_original $filelist

cp /vols/Data/km/cetisca/scripts/diffpostproc_masterfiles/acp.txt ${outputdir}/acp.txt
cp /vols/Data/km/cetisca/scripts/diffpostproc_masterfiles/index.txt ${outputdir}/index.txt
cp /vols/Data/km/cetisca/scripts/diffpostproc_masterfiles/topup_mouse.cnf ${outputdir}/topup_mouse.cnf

echo loading bvecs and bvals
cp ${inputdir}/${shell1}/method ${outputdir}/method_shell1
cp ${inputdir}/${shell2}/method ${outputdir}/method_shell2
cp ${inputdir}/${blipDown}/pdata/1/nifti/${substring}_${blipDown}_1_1.nii ${outputdir}/b0_blipDown.nii
/vols/Data/km/cetisca/functions/extract_bvecs_bvals.sh ${outputdir}

echo queuing orientation correction and mask generation
jid1=`fsl_sub -q veryshort.q -l ${outputdir}/logs1 /vols/Data/km/cetisca/scripts/diffpostproc_step1.sh ${outputdir}`
echo $jid1

echo queuing gibbs correction
#jid2=`/vols/Data/postmort/dSSFP_process/bin/dSSFP_preprocess/gibbs_correction_4D_clust ${outputdir}/data_gibbs.nii.gz -i ${outputdir}/data.nii.gz -l ${outputdir}/gibbslogs -d 1 -j $jid1 -nc -nd 64`
jid2=`fsl_sub -q short.q -j $jid1 -l ${outputdir}/ deGibbs3D ${outputdir}/data.nii.gz ${outputdir}/data_gibbs.nii.gz `
echo $jid2

echo queuing pretopup file processing
jid3=`fsl_sub -q veryshort.q -l ${outputdir}/logs3 -j $jid2 /vols/Data/km/cetisca/scripts/diffpostproc_step3.sh ${outputdir}`
echo $jid3

echo queuing topup
jid4=`fsl_sub -q cuda.q -j $jid3 topup --imain=${outputdir}/b0_topup --datain=${outputdir}/acp.txt --config=${outputdir}/topup_mouse.cnf --out=${outputdir}/topup_mouse_output_2b0 --fout=${outputdir}/topup_mouse_field_2b0 --iout=${outputdir}/topup_mouse_unwarped_images_2b0 --logout=${outputdir}/topup_mouse_2b0.log --verbose`
echo $jid4

echo queuing post-topup file orientations
jid5=`fsl_sub -q veryshort.q -l ${outputdir}/logs4 -j $jid4 /vols/Data/km/cetisca/scripts/diffpostproc_step4.sh ${outputdir}`
echo $jid5

echo queuing applytopup
jid6=`fsl_sub -q veryshort.q -l ${outputdir}/logsapplytopup -j $jid5 applytopup --imain=${outputdir}/data_gibbs_cp --datain=${outputdir}/acp.txt --inindex=1 --topup=${outputdir}/topup_mouse_output_2b0 --method=jac --out=${outputdir}/data_gibbs_applytopup`
echo $jid6

echo queuing eddy cuda
jid7=`fsl_sub -q cuda.q -j $jid5 eddy_cuda8.0 --imain=${outputdir}/data_gibbs_cp --mask=${outputdir}/b0_mean_mask_test.nii.gz --acqp=${outputdir}/acp.txt --index=${outputdir}/index.txt --bvecs=${outputdir}/bvecs_eddy --bvals=${outputdir}/bvals --topup=${outputdir}/topup_mouse_output_2b0 --out=${outputdir}/data_gibbs_eddy --verbose`
echo $jid7

echo queuing final file manipulations/organisations
jid8=`fsl_sub -q short.q -l ${outputdir}/logs5 -j $jid7 /vols/Data/km/cetisca/scripts/diffpostproc_step5.sh ${outputdir}`
echo $jid8
