#!/bin/sh

# Wrapper function to run NODDI
# Can use specify both dax and diso on either
# AH Apr 2020

make_absolute(){
    dir=$1;
    if [ -d ${dir} ]; then
	OLDWD=`pwd`
	cd ${dir}
	dir_all=`pwd`
	cd $OLDWD
    else
	dir_all=${dir}
    fi
    echo ${dir_all}
}
Usage() {
    echo ""
    echo "Usage: run_NODDI.sh <subject_directory> [options]"
    echo ""
    echo "expects to find data and nodif_brain_mask in subject directory"
    echo ""
    echo "<options>:"
    echo "-m (model = invivo/exvivo)"
    echo "--dax  (axial diffusivity) "
    echo "--diso (isotropic diffusivity) "
    echo "--runMCMC (if you want to run MCMC)"
    echo "--rician (if you want to run rician noise modelling)"
    echo "-b (burnin period, default 5000)"
    echo "-j (number of jumps, default 1250)"
    echo "-s (sample every, default 25)"
    echo "--BIC_AIC (calculate BIC & AIC)"
    echo ""
    exit 1
}


[ "$1" = "" ] && Usage

subjdir=`make_absolute $1`

module add cuda/8.0
. /opt/fmrib/fsl/etc/fslconf/fsl.sh
export FSLCONFDIR=/opt/fmrib/fsl/config
export FSLMACHTYPE=linux_64-gcc4.8
export CUDA=/opt/cuda-8.0
export FSLDEVDIR=/home/fs0/amyh/cudimot/fsl-dev
export CUDIMOT=$FSLDEVDIR

#parse option arguments
njobs=4
burnin=1000
njumps=1250
sampleevery=25
other=""
queue=""
lastStepModelOpts=""
model=invivo

shift
while [ ! -z "$1" ]
do
  case "$1" in
      -m) model=$2;shift;;
      --dax) dax=$2;shift;;
      --diso) diso=$2;shift;;
      -b) burnin=$2;shift;;
      -j) njumps=$2;shift;;
      -s) sampleevery=$2;shift;;
      --runMCMC) lastStepModelOpts=$lastStepModelOpts" --runMCMC";;
      --rician) lastStepModelOpts=$lastStepModelOpts" --rician";;
      --BIC_AIC) lastStepModelOpts=$lastStepModelOpts" --BIC_AIC";;
      *) other=$other" "$1;;
  esac
  shift
done

if [ -z "$model" ]; then
    echo "Must specify model name as either 'invivo' or 'exvivo'"
    exit 1
elif [ "$model" == "invivo" ]; then
    # Default params
    if [ -z "$dax" ]; then dax=0.0017; fi
    if [ -z "$diso" ]; then diso=0.003; fi
    export modelname=NODDI_Watson_diff
elif [ "$model" == "exvivo" ]; then
    # Default params
    if [ -z "$dax" ]; then dax=0.0006; fi       # No idea where these values come from
    if [ -z "$diso" ]; then diso=0.002; fi
    export modelname=NODDI_Watson_diff_exvivo
else
    echo "Must specify model name as either 'invivo' or 'exvivo'"
    exit 1
fi

opts="--dax $dax --diso $diso -b $burnin -j $njumps -s $sampleevery"

/home/fs0/amyh/cudimot/mymodels/$modelname/Pipeline_${modelname}.sh $subjdir $opts $lastStepModelOpts
