#!/bin/sh

# Wrapper function to run NODDI
# AH Apr 2020

make_absolute(){
    dir=$1;
    if [ -d ${dir} ]; then
	OLDWD=`pwd`
	cd ${dir}
	dir_all=`pwd`
	cd $OLDWD
    else
	dir_all=${dir}
    fi
    echo ${dir_all}
}
Usage() {
    echo ""
    echo "Usage: run_NODDI.sh <subject_directory>"
    echo ""
    echo "expects to find estimated parameters in subject directory"
    echo ""
    echo "<options>:"
    echo "-m (model = invivo/exvivo)"
    exit 1
}


[ "$1" = "" ] && Usage

subjdir=`make_absolute $1`

model=invivo # default

shift
while [ ! -z "$1" ]
do
  case "$1" in
      -m) model=$2;shift;;
  esac
  shift
done

if [ -z "$model" ]; then 
    echo "Must specify model name as either 'invivo' or 'exvivo'"
    exit 1
elif [ "$model" == "invivo" ]; then
    export modelname=NODDI_Watson_diff
elif [ "$model" == "exvivo" ]; then
    export modelname=NODDI_Watson_diff_exvivo
else
    echo "Must specify model name as either 'invivo' or 'exvivo'"
    exit 1
fi
      

outDir=${subjdir}.${modelname}

echo 'Converting cudimot output to NODDI variables'
/home/fs0/amyh/cudimot/mymodels/NODDI_Watson_diff/NODDI_Watson_finish.sh $outDir

