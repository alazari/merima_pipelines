#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/myelin/Mohamed/Merima_data_firstbatch_imported
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts/

subjList="MS81_10_20200904"

# "MS81_10_20200904
# MS82_3_20200914
# MS86_10_20200924
# MS81_1_20200901
# MS82_N_20200911
# MS86_1_20200921
# MS81_3_20200903
# MS84_10_20200918
# MS86_3_20200923
# MS81_N_20200907
# MS84_1_20200916
# MS86_N_20200925
# MS82_10_20200915
# MS84_3_20200917
# MS82_1_20200910
# MS84_N_20200909"

for subj in $subjList; do

  echo "preprocessing diffusion for $subj"

  # merge all scans

  for i in {1..32}; do
    printf "$workDir/"$subj"/DWI2/*_1_${i}.nii "  >> $workDir/"$subj"/DWI_filenames
  done

  for i in {1..32}; do
    printf "$workDir/"$subj"/DWI3/*_1_${i}.nii "  >> $workDir/"$subj"/DWI_filenames
  done

  filelist=`cat $workDir/"$subj"/DWI_filenames`

  fslmerge -t $workDir/data_merged $filelist



  # cp $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$numberT2w"/pdata/1/nifti/*.nii  $outDir/"$subj"/T2w/"$subj"_T2w_anat.nii
  #
  # cp $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$numberQSM"/pdata/1/nifti/*.nii $outDir/"$subj"/QSM
  #
  #
  # cp $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$numberDWI1"/pdata/1/nifti/*.nii $outDir/"$subj"/DWI1
  #
  # cp $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$numberDWI2"/pdata/1/nifti/*.nii $outDir/"$subj"/DWI2
  #
  #
  # cp $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$numberDWI3"/pdata/1/nifti/*.nii $outDir/"$subj"/DWI3


echo "done"

done
