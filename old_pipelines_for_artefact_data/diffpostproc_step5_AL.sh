#!/bin/bash

inputfolder=$1

#5th step: dtifit

#dtifit
mkdir ${inputfolder}/dtifit
dtifit -k ${inputfolder}/data.nii.gz -m ${inputfolder}/b0_mean_mask.nii.gz -b ${inputfolder}/bvals -r ${inputfolder}/bvecs_zminxy -o ${inputfolder}/dtifit_gibbs_eddy/dtifit_gibbs_eddy


# fslswapdim ${inputfolder}/data_gibbs_applytopup x -z y ${inputfolder}/data_gibbs_applytopup
# fslswapdim ${inputfolder}/data_gibbs_eddy x -z y ${inputfolder}/data_gibbs_eddy
#
# #5th step: dtifit
#
# #dtifit
# mkdir ${inputfolder}/dtifit_gibbs_eddy
# dtifit -k ${inputfolder}/data_gibbs_eddy.nii.gz -m ${inputfolder}/b0_mean_mask.nii.gz -b ${inputfolder}/bvals -r ${inputfolder}/bvecs_zminxy -o ${inputfolder}/dtifit_gibbs_eddy/dtifit_gibbs_eddy

# mkdir ${inputfolder}/data_noddi_bpx
# cp ${inputfolder}/data_gibbs_eddy.nii.gz ${inputfolder}/data_noddi_bpx/data.nii.gz
# cp ${inputfolder}/b0_mean_mask.nii.gz ${inputfolder}/data_noddi_bpx/nodif_brain_mask.nii.gz
# cp ${inputfolder}/bvals ${inputfolder}/data_noddi_bpx/bvals
# cp ${inputfolder}/bvecs_zminxy ${inputfolder}/data_noddi_bpx/bvecs

# #check data is okay
# bedpostx_datacheck ${inputfolder}/data_noddi_bpx
#
# #6th step: bedpostx
# bedpostx ${inputfolder}/data_noddi_bpx

#6th step: noddi

#/home/fs0/amyh/func/run_NODDI.sh ${inputfolder}/data_noddi_bpx -m exvivo --dax 0.00056 --diso 0.001 --runMCMC
