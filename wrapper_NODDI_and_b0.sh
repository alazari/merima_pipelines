#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts/

subjList="20210930_183625_MS02_MS02_Merima_Doldcr_T2w_Diffusion_1_1
20211004_193158_MS03_MS03_Merima_Doldcr_T2w_Diffusion_1_1
20211006_163600_MS04b_MS04_Merima_Doldcr_T2w_Diffusion_1_1
20211007_161034_MS06_MS06_Merima_Doldcr_T2w_Diffusion_1_1
20211008_154740_MS07_Merima_Doldcr_T2w_Diffusion_1_1
20211011_201821_MS08_Merima_Doldcr_T2w_Diffusion_1_1
20211012_174125_MS09_Merima_Doldcr_T2w_Diffusion_1_2
20211013_180206_MS10_Merima_Doldcr_T2w_Diffusion_1_1
20211014_162422_MS11_Merima_Doldcr_T2w_Diffusion_1_1
20211015_180125_MS12_Merima_Doldcr_T2w_Diffusion_1_1
20211018_175940_MS13_Merima_Doldcr_T2w_Diffusion_1_2
20211019_174300_MS14_Merima_Doldcr_T2w_Diffusion_1_2
20211021_200506_MS15_Merima_Doldcr_T2w_Diffusion_1_1
20211022_151312_MS16_Merima_Doldcr_T2w_Diffusion_1_2
20211026_172625_MS17_Merima_Doldcr_T2w_Diffusion_1_1
20211027_203821_MS18_Merima_Doldcr_T2w_Diffusion_1_1
20211029_154024_MS19b_Merima_Doldcr_T2w_Diffusion_1_1
20211108_194315_MS22_Merima_Doldcr_T2w_Diffusion_4_1_5
20211103_181935_MS23_Merima_Doldcr_T2w_Diffusion_1_1
20211104_155453_MS25_Merima_Doldcd_T2w_Diffusion_1_1
20211109_155744_MS26_Merima_Doldcr_T2w_Diffusion_1_1"

#"20210312_192904_MS80_Merima_Doldcr_T2w_Diffusion_1_3"


for subj in $subjList; do

  echo "running NODDI for $subj"

  outDir=/vols/Scratch/myelin/Mohamed/Merima_SecondExperiment_firstbatch_preprocessed/"$subj"

  # produce average b0
  select_dwi_vols $outDir/data_gibbs_eddy.nii.gz $outDir/bvals $outDir/data_gibbs_eddy_b0_mean.nii.gz 0 -m

  # run NODDI
  /home/fs0/amyh/func/run_NODDI.sh $outDir/data_noddi_bpx -m exvivo --dax 0.00056 --diso 0.001 –runMCMC

  # run NODDI with updated dax assumptions + Rician bias correction
  #/home/fs0/amyh/func/run_NODDI.sh $outDir/data_noddi_bpx -m exvivo --dax 0.00047 --diso 0.001 --runMCMC —rician

  # clean up NODDI and calculate parametric maps
  #/vols/Data/km/cetisca/scripts/diff_cleanup.sh $outDir

echo "done"

done
